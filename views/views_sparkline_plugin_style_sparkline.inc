<?php
/**
 * @file
 * Contains the sparkline style plugin.
 */

/**
 * Style plugin to render a sparkline style graph for the results.
 *
 * @ingroup views_style_plugins
 */
class views_sparkline_plugin_style_sparkline extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['width'] = array('default' => '100px');
    $options['height'] = array('default' => '30px');

    $options['series'] = array(
      'contains' => array(),
    );

    for ($i = 1; $i <= 5; $i ++) {
      $options['series']['contains'][$i] = array(
        'contains' => array(
          'color' => array('default' => '#336699'),
          'type' => array('default' => 'line'),
          'field' => array('default' => NULL),
          'enable' => array('default' => FALSE),
          'label_point' => array('default' => FALSE),
          'label_point_color' => array('default' => '#FF0000'),
          'label_point_fill_color' => array('default' => '#FF0000'),
          'label_point_radius' => array('default' => '1'),
          'label_point_left_offset' => array('default' => '2'),
          'label_point_top_offset' => array('default' => '-10'),
          'shadow_size' => array('default' => '0'),
          'line_width' => array('default' => '1.0'),
          'line_fill' => array('default' => FALSE),
          'bar_width' => array('default' => 0.8),
          'bar_align' => array('default' => 'center'),
          'pad' => array('default' => 'none'),
          'pad_value' => array('default' => 'null'),
        )
      );
    }

    return $options;
  }

  /**
   * Get a list of fields to use for series selection.
   */
  function get_fields() {
    $sanitized = array();
      $fields = $this->display->handler->get_option('fields');

    // Preconfigure the sanitized array so that the order is retained.
    foreach ($fields as $field => $info) {
      // Set to itself so that if it isn't touched, it gets column
      // status automatically.
      $sanitized[$field] = $field;
    }

    return $sanitized;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    $columns = $this->get_fields();

    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $this->options['width'],
      '#description' => t("Must include a unit such as '60px' or '100%'"),
    );
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => $this->options['height'],
      '#description' => t("Must include a unit such as '60px'"),
    );

    foreach ($this->options['series'] as $key => $options) {
      $form['series'][$key]['enable'] = array(
        '#type' => 'checkbox',
        '#title' => t('Series @key: enabled', array('@key' => $key)),
        '#default_value' => $this->options['series'][$key]['enable'],
      );
      $form['series'][$key]['field'] = array(
        '#type' => 'select',
        '#title' => t('Field'),
        '#options' => $columns,
        '#default_value' => $this->options['series'][$key]['field'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array("edit-style-options-series-$key-enable" => array(1)),
      );
      $form['series'][$key]['type'] = array(
        '#type' => 'select',
        '#title' => t('Type'),
        '#options' => array('line' => t('Line'), 'bar' => t('Bar')),
        '#default_value' => $this->options['series'][$key]['type'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array("edit-style-options-series-$key-enable" => array(1)),
      );

      $form['series'][$key]['color'] = array(
        '#type' => 'textfield',
        '#title' => t('Color'),
        '#description' => t('A color to use for the chart in hexadecimal. For example \'#336699\''),
        '#default_value' => $this->options['series'][$key]['color'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array("edit-style-options-series-$key-enable" => array(1)),
      );
      $form['series'][$key]['line_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Line width'),
        '#description' => t('Line width in pixels. For example \'1.5\''),
        '#default_value' => $this->options['series'][$key]['line_width'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-type" => array('line'),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['line_fill'] = array(
        '#type' => 'checkbox',
        '#title' => t('Line fill'),
        '#description' => t('Fill area under line'),
        '#default_value' => $this->options['series'][$key]['line_fill'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-type" => array('line'),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['bar_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Bar width'),
        '#description' => t('Bar width in terms of graph units. For example \'1\' is equal to one unit of width along the x-axis.'),
        '#default_value' => $this->options['series'][$key]['bar_width'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-type" => array('bar'),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['bar_align'] = array(
        '#type' => 'select',
        '#title' => t('Bar alignment'),
        '#default_value' => $this->options['series'][$key]['bar_align'],
        '#process' => array('views_process_dependency'),
        '#options' => array('left' => t('Left'), 'center' => t('Center'), 'right' => t('Right')),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-type" => array('bar'),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['shadow_size'] = array(
        '#type' => 'textfield',
        '#title' => t('Shadow size'),
        '#default_value' => $this->options['series'][$key]['shadow_size'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
        ),
      );
      $form['series'][$key]['label_point'] = array(
        '#type' => 'checkbox',
        '#title' => t('Label last point'),
        '#default_value' => $this->options['series'][$key]['label_point'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
        ),
      );
      $form['series'][$key]['label_point_radius'] = array(
        '#type' => 'textfield',
        '#title' => t('Radius of last point'),
        '#default_value' => $this->options['series'][$key]['label_point_radius'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-label-point" => array(1),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['label_point_color'] = array(
        '#type' => 'textfield',
        '#title' => t('Color of last point'),
        '#default_value' => $this->options['series'][$key]['label_point_color'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-label-point" => array(1),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['label_point_fill_color'] = array(
        '#type' => 'textfield',
        '#title' => t('Fill color of last point'),
        '#default_value' => $this->options['series'][$key]['label_point_fill_color'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-label-point" => array(1),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['label_point_left_offset'] = array(
        '#type' => 'textfield',
        '#title' => t('Label left offset'),
        '#description' => t('Offset of the label of the last point in pixels. Can be positive or negative.'),
        '#default_value' => $this->options['series'][$key]['label_point_left_offset'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-label-point" => array(1),
        ),
        '#dependency_count' => 2
      );
      $form['series'][$key]['label_point_top_offset'] = array(
        '#type' => 'textfield',
        '#title' => t('Label top offset'),
        '#description' => t('Offset of the label of the last point in pixels. Can be positive or negative.'),
        '#default_value' => $this->options['series'][$key]['label_point_top_offset'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-label-point" => array(1),
        ),
        '#dependency_count' => 2
      );

      $form['series'][$key]['pad'] = array(
        '#type' => 'select',
        '#title' => t('Pad'),
        '#options' => array('none' => t('None'), 'left' => t('Left'), 'right' => t('Right')),
        '#description' => t('If the view returns less rows than the number of items to display, optionally pad the view with null points to the left or right of the returned rows.'),
        '#default_value' => $this->options['series'][$key]['pad'],
        '#process' => array('views_process_dependency'),
        '#dependency' => array("edit-style-options-series-$key-enable" => array(1)),
      );
      $form['series'][$key]['pad_value'] = array(
        '#type' => 'select',
        '#title' => t('Pad value'),
        '#default_value' => $this->options['series'][$key]['pad_value'],
        '#options' => array('null' => t('Null'), 'zero' => t('Zero')),
        '#process' => array('views_process_dependency'),
        '#dependency' => array(
          "edit-style-options-series-$key-enable" => array(1),
          "edit-style-options-series-$key-pad" => array('left', 'right'),
        ),
        '#dependency_count' => 2
      );
    }

  }
}
