<?php
/**
 * @file views-sparkline.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="sparkline">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>

 <?php print $data_script; ?>
 <?php print $sparkline; ?>
 <?php print $label_script; ?>
</div>
